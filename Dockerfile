FROM openjdk:8-jdk-alpine
COPY target/devops-sean.jar /tmp/devops-sean.jar
ENTRYPOINT ["java","-Xms4g","-Xmx4g","-agentlib:jdwp=transport=dt_socket,address=7654,server=y,suspend=n","-Djava.security.egd=file:/dev/./urandom","-jar","/tmp/devops-sean.jar"]