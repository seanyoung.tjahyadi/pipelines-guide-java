package com.izeno.sean.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.izeno.sean.student.Student;
import com.izeno.sean.student.StudentRepository;

@RequestMapping("v1")
@RestController
public class StudentResource {
	@Autowired
	private StudentRepository studentRepository;

	@GetMapping("/students")
	public List<Student> retrieveAllStudents() {
		List<Student> students = studentRepository.findAll();
		System.out.println("Total : "+students.size());
		return students;
	}

	@GetMapping("/students/{id}")
	public Student retrieveStudent(@PathVariable long id) {
		Student student = studentRepository.findById(id);
		return student;
	}
	
	@RequestMapping(value = "/create/{count}", method=RequestMethod.GET)
	public ResponseEntity<Object> dumpStudent(@PathVariable int count) {
		Student latest = studentRepository.findLatest();
		Long startId = latest.getId();
		for (int i = 1; i <= count; i++) {
			studentRepository.insert(new Student(startId+i, "John"+i, "A1234657"));
		}
		return ResponseEntity.noContent().build();

	}

	@DeleteMapping("/students/{id}")
	public void deleteStudent(@PathVariable long id) {
		studentRepository.deleteById(id);
	}
	
	@GetMapping("/environment")
	public Map<String, Object> environment() throws FileNotFoundException, IOException {
		Properties additionalProperties = new Properties();
		//String cp = System.getenv("CONFIGURATION_MOUNT_PATH");
		//additionalProperties.load(new java.io.FileInputStream(cp));
		String ret = "A";//additionalProperties.getProperty("environment.key");
		String color = "000000";//additionalProperties.getProperty("environment.color");
		Map<String, Object> resp = new LinkedHashMap<String, Object>();
		resp.put("payload", ret);
		resp.put("color", color);
		return resp;
	}

	@PostMapping("/students")
	public ResponseEntity<Object> createStudent(@RequestBody Student student) {
		// Student savedStudent = studentRepository.save(student);
		int id = studentRepository.insert(new Student(1003L, "John", "A1234657"));

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();

		return ResponseEntity.created(location).build();

	}

	@PutMapping("/students/{id}")
	public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable long id) {

		Student studentOptional = studentRepository.findById(id);

		studentOptional.setName("HAHA");

		studentRepository.update(studentOptional);

		return ResponseEntity.noContent().build();
	}
}
